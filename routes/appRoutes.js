var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();

    //Welcome Routes
    var welcomeCtrl = require('./../controllers/welcomeCtrl');
    router.get('/home',welcomeCtrl.showHomePage);
    router.get('/aboutus',welcomeCtrl.showAboutusPage);
    router.get('/ourteam',welcomeCtrl.showOurteamPage);
    router.get('/acknowledgements',welcomeCtrl.showAcknowledgementPage);
    
  var userCtrl = require('./../controllers/userCtrl');
   router.get('/customer/:id', userCtrl.showCustomerDetailPage);
   router.get('/customer',userCtrl.showAllCustomers);
   router.get('/vendor/:u_id',userCtrl.showVendorDetailPage);
   router.get('/vendor',userCtrl.showVendorsPage);
 
   router.post('/login',userCtrl.login);

   router.post('/changepassword',userCtrl.changepassword);


    router.post('/editprofile',userCtrl.editprofile);
    router.post('/editvendorprofile',userCtrl.editvendorprofile);

   
    router.get('/home/signupasvendor',userCtrl.showsignupasVendorPage);
  router.post('/signupasVendor',userCtrl.signup);
  router.post('/signupascustomer',userCtrl.signupascustomer );

  router.get('/admin',userCtrl.showAdminPage);
  router.get('/profile/:id',userCtrl.showprofilepage);


  router.get('/logout',userCtrl.logout);
  
  router.post('/checkmob',userCtrl.checkmobs);
  
  router.post('/checkfirm',userCtrl.checkfirmname);

   var searchCtrl = require('./../controllers/searchCtrl');
   router.get('/search', searchCtrl.showSearchPage);
   var productCtrl = require('./../controllers/productCtrl');
   router.post('/disableproduct',productCtrl.disableProduct);
   router.get('/product',productCtrl.showAllProducts);
   router.get('/product/:id',productCtrl.showProductDetailPage);
   router.get('/:slug/addproduct',productCtrl.addproductPage);
   router.post('/addproduct',productCtrl.showaddproductPage);
  router.get('/product/addreview/:id',productCtrl.showAddReviewPage);
  router.post('/product/addingreview/:add/:id',productCtrl.addReviewPage);
  router.post('/feedback',productCtrl.showFeedbackPage);
 
  var verify= require('./../controllers/varificationandcontactCtrl');
  router.post('/checkproduct',productCtrl.checkproduct);
router.get('/forgotpass',welcomeCtrl.forgotpassword);
router.post('/send',verify.sendotp);
router.post('/newpass',welcomeCtrl.checkotp);
router.post('/reset',welcomeCtrl.pass);
    return router.middleware();
}
