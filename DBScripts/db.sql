-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: newbee
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL,
  `u_id` int(10) unsigned NOT NULL,
  `feedback` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `creation_timestamp` timestamp NOT NULL,
  KEY `review_idx` (`id`),
  KEY `user_5_idx` (`u_id`),
  CONSTRAINT `review` FOREIGN KEY (`id`) REFERENCES `review` (`id`),
  CONSTRAINT `user_5` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES (108,238,'dislike','2018-07-18 09:42:32'),(111,238,'dislike','2018-07-18 06:03:29'),(112,240,'like','2018-07-20 07:05:12'),(108,101,'like','2018-07-20 07:18:27'),(111,101,'like','2018-07-20 07:18:32'),(118,102,'dislike','2018-07-20 07:30:33'),(115,102,'like','2018-07-21 08:52:11'),(108,102,'like','2018-07-20 08:41:06'),(108,100,'like','2018-07-21 08:44:49'),(110,100,'like','2018-07-21 04:13:40'),(125,100,'like','2018-07-21 04:03:12'),(115,100,'inappropriate','2018-07-21 08:44:55'),(111,100,'spam','2018-07-21 05:47:32'),(126,100,'like','2018-07-21 05:50:19'),(126,102,'spam','2018-07-21 06:54:53'),(120,100,'inappropriate','2018-07-24 08:31:16'),(128,100,'inappropriate','2018-07-21 07:43:11'),(118,100,'like','2018-07-24 08:30:58'),(116,100,'spam','2018-07-21 08:07:47'),(111,102,'like','2018-07-21 08:41:20'),(130,100,'like','2018-07-21 08:45:06'),(135,100,'like','2018-07-24 08:31:28');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otp`
--

DROP TABLE IF EXISTS `otp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `otp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `otp` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otp`
--

LOCK TABLES `otp` WRITE;
/*!40000 ALTER TABLE `otp` DISABLE KEYS */;
/*!40000 ALTER TABLE `otp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `product` (
  `p_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `image_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `product_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `faq` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `available` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`p_id`),
  UNIQUE KEY `NAME_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1020 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1000,'bottles','lorem','\\static\\uploads\\bottles.jpg','/bottle.com','dummy data.txt',1),(1001,'namkeen','lorem','\\static\\uploads\\namkeen.jpg','/namkeen.com','dummy data.txt',1),(1002,'soap','lorem','\\static\\uploads\\soap.jpg','/soap.com','dummy data.txt',1),(1003,'biscuit','These are home-made biscuits','\\static\\uploads\\biscuit.jpg','/buiscit.com','dummy data.txt',1),(1004,'keyboard','lorem','\\static\\uploads\\keyboard.jpg','/keyboard.com','dummy data.txt',1),(1005,'mouse','lorem','\\static\\uploads\\mouse.jpg','/mouse.com','dummy data.txt',1),(1006,'monitor','lorem','\\static\\uploads\\monitor.jpg','/monitor.com','dummy data.txt',1),(1007,'mobile','lorem','\\static\\uploads\\mobile.jpg','/mobile.com','dummy data.txt',1),(1008,'pen','lorem','\\static\\uploads\\pen.jpg','/pen.com','dummy data.txt',1),(1009,'register','lorem','\\static\\uploads\\register.png','/register.com','dummy data.txt',1),(1010,'laptop','lorem','\\static\\uploads\\laptop.jpg','/laptop.com','dummy data.txt',1),(1017,'bhujia','namkeen','\\static\\uploads\\bhujia.jpg','app//product//1017','static\\static\\uploads\\upload_c44d2d2fd1f6ed9e817a83fabb204dda',1),(1018,'Crispy chips','These are very tasty chips which comes in different flavours. ','\\static\\uploads\\crispy chips.png','app/product/1018','\\static\\uploads\\upload_48c2ace11f03084d87e1a84b93def7f2.txt',1),(1019,'Cake','This cake comes in all flavours.','\\static\\uploads\\upload_ff649e6c78c1fae45ca9d96ee431b1e9.jpeg','app/product/1019','\\static\\uploads\\upload_33b83433e0971f7afa1fc08e921502fd',1);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `review` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_id` int(10) unsigned NOT NULL,
  `u_id` int(10) unsigned NOT NULL,
  `rating` float DEFAULT NULL,
  `review` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `creation_timestamp` timestamp NOT NULL,
  `verified` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `product_idx` (`p_id`),
  KEY `user_4_idx` (`u_id`),
  CONSTRAINT `product_1` FOREIGN KEY (`p_id`) REFERENCES `product` (`p_id`),
  CONSTRAINT `user_4` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (108,1018,238,2.5,'This is a nice product. The chips are good.','2018-07-14 08:39:23',1),(109,1017,238,2.5,'nice','2018-07-14 08:40:32',0),(110,1017,238,3,'','2018-07-14 08:40:44',1),(111,1018,239,3.5,'nice','2018-07-17 04:27:19',1),(112,1008,240,3.5,'nice namkeen','2018-07-20 05:12:55',1),(113,1010,101,4,'works fast','2018-07-20 07:16:35',1),(114,1004,101,2.5,'keys are slow and hard','2018-07-20 07:17:43',1),(115,1018,101,4,'wonderful','2018-07-20 07:18:18',1),(116,1007,101,4,'like that','2018-07-20 07:19:21',1),(117,1000,101,5,'nice plastic','2018-07-20 07:21:09',1),(118,1019,101,4,'chocolate flavor is my fav!','2018-07-20 07:22:11',1),(119,1010,102,3.5,'good','2018-07-20 07:25:48',1),(120,1019,102,0.5,'bad','2018-07-20 07:29:41',1),(121,1005,102,5,'works very smooth','2018-07-20 07:31:16',1),(122,1002,102,4,'nice','2018-07-20 07:32:55',1),(123,1003,102,2.5,'average','2018-07-20 08:28:45',1),(124,1017,102,3.5,'','2018-07-20 08:38:06',0),(125,1017,102,3,'nice','2018-07-20 08:38:25',0),(126,1018,102,2,'nice','2018-07-20 08:40:41',0),(127,1019,100,3.5,'this a veryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryveryv','2018-07-21 04:56:04',0),(128,1019,100,2.5,'this is a good review this is a good review this is a good review this is a good review this is a good review this is a good review','2018-07-21 05:02:42',0),(129,1017,102,2.5,'edited','2018-07-21 05:12:26',1),(130,1018,100,3.5,'This is a very good product This is a very good product This is a very good productThis is a very good product This is a very good product This is a very good product  ','2018-07-21 05:59:56',1),(131,1018,102,2.5,'edited yo','2018-07-21 07:45:41',1),(132,1007,241,2.5,'Fast and cool','2018-07-24 04:02:39',1),(133,1018,241,2.5,'Crispy','2018-07-24 04:06:20',0),(134,1018,241,2.5,'oh yes it is very crispy','2018-07-24 04:11:50',1),(135,1019,100,2.5,'good','2018-07-24 08:30:46',1),(136,1001,101,3.5,'Mast','2018-07-24 11:10:58',1);
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `role` (
  `r_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`r_id`),
  UNIQUE KEY `NAME_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (3,'admin'),(1,'customer'),(2,'vendor');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `user` (
  `u_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mob` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `img` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `creation_timestamp` timestamp NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `user_email` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `user_email_UNIQUE` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (100,'Gagan Gaur','9876543210','/static/uploads/gagan.jpeg','1998-06-17','m','gagan','2018-06-23 09:52:31',1,'gagan.gaur_cs16@gla.ac.in'),(101,'kumud','9876543211','/static/uploads/kumud.jpg','1998-06-17','m','kumud','2018-06-23 09:52:31',1,'kumud.gupta_cs16@gla.ac.in'),(102,'Akrati Chaturvedi','987654322','/static/uploads/akrati.jpg','1998-06-17','f','akrati','2018-06-23 09:52:32',1,'sajal.agarwal_ca16@gla.ac.in'),(103,'kunal','9876543213','//www.xyz.com','1998-06-17','f','kunal','2018-06-23 09:52:32',1,'kunal.agarwal_ca16@gla.ac.in'),(104,'kumar','9876543214','/static/uploads/kumar.jpg','1998-06-17','m','kumar','2018-06-23 09:52:32',1,'mud.gupta_cs16@gla.ac.in'),(105,'sanam','9876543215','/static/uploads/sanam.jpg','1998-06-17','f','sanam','2018-06-23 09:52:32',1,'jal.agarwal_ca16@gla.ac.in'),(106,'ankit','1212378286','//www.xyz.com','1998-06-17','f','jgdfhgdf','2018-06-23 09:52:32',1,'nal.agarwal_ca16@gla.ac.in'),(107,'piyush','9876543217','/static/uploads/piyush.jpg','1998-06-17','m','piyush','2018-06-23 09:52:33',1,'kumud.gup_cs16@gla.ac.in'),(108,'arpit','9639401042','/static/uploads/arpit.jpg','1997-02-18','f','arpit','2018-06-23 09:52:33',1,'arpit.gupta_cs16@gla.ac.in'),(109,'krishna','1212755286','//www.xyz.com','1998-06-17','f','jgdfhgdf','2018-06-23 09:52:33',1,'kunal.agar_ca16@gla.ac.in'),(238,'Sajal Agrawal','8218814194','/static/uploads/sajal.jpg','1998-10-07','f','qwerty','2018-07-13 05:17:51',1,'sajal10798@gmail.com'),(239,'Rajat Agrawal','8755604481','/static/uploads/rajat.jpg','1996-04-06',NULL,'rajat','2018-07-13 05:22:22',1,'rajat@gmail.com'),(240,'Manika Agrawal','9987654321','\\static\\uploads\\upload_61d59b10ecc6ba6940908d3e88beb0b2.JPG',NULL,NULL,'manika','2018-07-20 05:10:39',1,'manika@gmail.com'),(241,'Ansha Sharan','8445178234','\\static\\uploads\\upload_40471fa0a666cad1aee2a3c6ff58edf4.jpg',NULL,NULL,'ansha','2018-07-24 04:01:02',1,'ansha@gmail.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u_id` int(10) unsigned NOT NULL,
  `r_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_idx` (`u_id`),
  KEY `role_idx` (`r_id`),
  CONSTRAINT `role` FOREIGN KEY (`r_id`) REFERENCES `role` (`r_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_1` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (200,100,2),(201,101,2),(202,102,2),(203,103,2),(204,104,2),(210,105,1),(211,106,1),(212,107,1),(213,108,1),(214,238,1),(215,239,2),(216,240,1),(217,241,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `vendor` (
  `u_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firm_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `location` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gst_no` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pan_no` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gst_certi` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `firm_name_UNIQUE` (`firm_name`),
  CONSTRAINT `user_3` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor`
--

LOCK TABLES `vendor` WRITE;
/*!40000 ALTER TABLE `vendor` DISABLE KEYS */;
INSERT INTO `vendor` VALUES (100,'VISHAL_MEGA_MART','KRISHNA NAGAR','11AAAAA0000A1A1','ABCDE1231A','HTTP://www.abc.com','\\static\\uploads\\vishal.png'),(101,'BIG_BAZAR','TOWNSHIP','11AAAAA0000A1B1','ABCDE1231B','HTTP://www.def.com','\\static\\uploads\\bigbazar.png'),(102,'EASY_DAY','ROOPAM','11AAAAA0000A1C1','ABCDE1231C','HTTP://www.ghi.com','\\static\\uploads\\easyday.png'),(103,'SR_DAILY_NEEDS','GOVIND NAGAR','11AAAAA0000A1D1','ABCDE1231D','HTTP://www.jkl.com','\\static\\uploads\\sr.jpg'),(104,'PANCHHI STORE','KACHCHI SADAK','11AAAAA0000A1E1','ABCDE1231E','HTTP://www.mno.com','\\static\\uploads\\panchi.jpg'),(239,'Brijwasi Store','Janmasthan, Mathura','29ABCDE1234F2Z5','AAAPL1234C','\\static\\uploads\\upload_7c1ca20da27d95da114bcfe9f0000282.jpg','\\static\\uploads\\brijwasi.jpg');
/*!40000 ALTER TABLE `vendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor_product`
--

DROP TABLE IF EXISTS `vendor_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `vendor_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creation_timestamp` timestamp NOT NULL,
  `u_id` int(10) unsigned NOT NULL,
  `p_id` int(10) unsigned NOT NULL,
  `available` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_idx` (`u_id`),
  KEY `product_idx` (`p_id`),
  CONSTRAINT `product` FOREIGN KEY (`p_id`) REFERENCES `product` (`p_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor_product`
--

LOCK TABLES `vendor_product` WRITE;
/*!40000 ALTER TABLE `vendor_product` DISABLE KEYS */;
INSERT INTO `vendor_product` VALUES (50,'2018-06-23 09:52:39',100,1000,0),(51,'2018-06-23 09:52:39',100,1005,1),(52,'2018-06-23 09:52:40',100,1007,1),(53,'2018-06-23 09:52:40',101,1002,1),(54,'2018-06-23 09:52:40',101,1005,1),(55,'2018-06-23 09:52:41',102,1004,1),(56,'2018-06-23 09:52:41',102,1008,1),(57,'2018-06-23 09:52:41',103,1010,1),(58,'2018-06-23 09:52:41',104,1009,1),(59,'2018-06-23 09:52:41',104,1002,1),(65,'2018-07-05 08:18:46',100,1017,1),(66,'2018-07-13 05:33:59',239,1018,0),(67,'2018-07-13 06:20:25',101,1018,1),(68,'2018-07-20 04:14:52',100,1019,1),(69,'2018-07-24 09:36:08',100,1018,0),(70,'2018-07-24 09:40:50',100,1018,0),(75,'2018-07-24 10:05:29',100,1008,1),(76,'2018-07-24 11:10:15',101,1001,1);
/*!40000 ALTER TABLE `vendor_product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-24 16:53:27
