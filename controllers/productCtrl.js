var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
const math = require('mathjs')
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
    showAllProducts: function*(next){
        var queryproduct = 'select product.p_id,name,image_url,count(*) as "rated_by" ,rating from product left join review on product.p_id=review.p_id and product.available=1 group by p_id order by p_id desc ;'
        var products_list = yield databaseUtils.executeQuery(queryproduct);
        for(var i in products_list)
        {
            //console.log("-----------"+i+"-------------"+products_list[i].p_id+"----------"+products_list[i].rated_by+"-----------"+products_list[i].rating);
            
            var productId = products_list[i].p_id;
            var queryproduct =  'select (select count(*) from review where p_id = "%s" and rating = 5 and verified=1)as r5,(select count(*) from review where p_id = "%s" and rating = 4.5 and verified=1)as r45,(select count(*) from review where p_id = "%s" and rating = 4 and verified=1)as r4,(select count(*) from review where p_id = "%s" and rating = 3.5 and verified=1)as r35,(select count(*) from review where p_id = "%s" and rating = 3 and verified=1)as r3,(select count(*) from review where p_id = "%s" and rating = 2.5 and verified=1)AS r25,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 2 and verified=1)as r2,(select count(*) from review where p_id = "%s" and rating = 1.5 and verified=1)AS r15,(select count(*) from review where p_id = "%s" and rating = 1 and verified=1) as r1,(select count(*) from review where p_id = "%s" and rating = 0.5 and verified=1) as r05;';
             var query = util.format(queryproduct,productId,productId,productId,productId,productId,productId,productId,productId,productId,productId);
           
            var result= yield databaseUtils.executeQuery(query);
            var credit=(result[0].r5)+(result[0].r4)+(result[0].r3)+(result[0].r2)+(result[0].r1)+(result[0].r45)+(result[0].r35)+(result[0].r25)+(result[0].r15)+(result[0].r05);
            if(credit){
          // console.log("r5"+(result[0].r5)+"r4"+(result[0].r4)+"r3"+(result[0].r3)+"r2"+(result[0].r2)+"r1"+(result[0].r1));
                 var avg= ((result[0].r5)*5 + (result[0].r4)*4 +(result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1+(result[0].r45)*4.5+(result[0].r35)*3.5+(result[0].r25)*2.5+(result[0].r15)*1.5+(result[0].r05)*0.5 )/(credit);
                 products_list[i].rating=avg;
                 products_list[i].rated_by=credit;
            }
             else 
             {
            products_list[i].rating=0;
            products_list[i].rated_by=0;
             }
              //   console.log("****"+products_list[i].rating);
        }
    yield this.render('products',{
        products_list: products_list
    })
    },
    showProductDetailPage: function*(next){
        var added=0;
        var reviewid=0;
        var productId= this.params.id;
        var queryString = 'select * from product where p_id = "%s" and available=1';
        var query = util.format(queryString, productId);
        var result = yield databaseUtils.executeQuery(query);
        var product_details = result[0];
        queryString = 'select review.id,review.u_id, name,img ,rating,review,feedback,count(feedback) AS count,review.creation_timestamp from (review inner join user on user.u_id=review.u_id ) left join feedback on review.id=feedback.id where p_id="%s" and verified = 1 GROUP BY id,feedback ORDER BY id,feedback';
        query= util.format(queryString, productId);
        result = yield databaseUtils.executeQuery(query);
        //console.log(result);
        var user_reviews=result;
        var fr = {};
        for(var ele in user_reviews){
        var id = user_reviews[ele]['id'];
        var uid = user_reviews[ele]['u_id'];
        var name = user_reviews[ele]['name'];
        var img = user_reviews[ele]['img'];
        var rating = user_reviews[ele]['rating'];
        var review = user_reviews[ele]['review'];
        var feedback = user_reviews[ele]['feedback'];
        var count = user_reviews[ele]['count'];
        var creation_timestamp = user_reviews[ele]['creation_timestamp'];
        var mydate = new Date(creation_timestamp);
       // console.log("original"+ creation_timestamp + "now" + mydate.toDateString());
        var date= mydate.toDateString();
        if(fr[id]==undefined){
        fr[id]={};
        fr[id]['uid']=uid;
        fr[id]['name']=name;
        fr[id]['img']=img;
        fr[id]['rating']=rating;
        fr[id]['review']=review;
        fr[id]['creation_timestamp']=date;
        fr[id][feedback]=count;
        }
        else{
        fr[id][feedback]=count;
        }
        }
        user_reviews_array=[];
        for(var i in fr){
        var tmp={};
        // console.log(i);
        tmp['id']=i;
        tmp['details']=fr[i];
        user_reviews_array.push(tmp);
        }
        user_reviews_array.reverse();
        console.log(user_reviews_array);
        console.log("----------------------------------***************************--------------------------------");
        if(this.currentUser){
        var cid= this.currentUser.u_id;
        //console.log(cid);
        for( var i in user_reviews_array ){
        if(user_reviews_array[i].details.uid == cid){
        added = 1;
        reviewid = i;
        break;
        }
        }
        // console.log(added);
        }
        
        queryString = 'SELECT * FROM vendor where u_id in (Select distinct(u_id) from product JOIN vendor_product ON product.p_id=vendor_product.p_id where product.p_id="%s")';
        query= util.format(queryString, productId);
        result = yield databaseUtils.executeQuery(query);
        var vendors_list= result;
        
        
       var queryproduct =  'select (select count(*) from review where p_id = "%s" and rating = 5 and verified=1)as r5,(select count(*) from review where p_id = "%s" and rating = 4.5 and verified=1)as r45,(select count(*) from review where p_id = "%s" and rating = 4 and verified=1)as r4,(select count(*) from review where p_id = "%s" and rating = 3.5 and verified=1)as r35,(select count(*) from review where p_id = "%s" and rating = 3 and verified=1)as r3,(select count(*) from review where p_id = "%s" and rating = 2.5 and verified=1)AS r25,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 2 and verified=1)as r2,(select count(*) from review where p_id = "%s" and rating = 1.5 and verified=1)AS r15,(select count(*) from review where p_id = "%s" and rating = 1 and verified=1) as r1,(select count(*) from review where p_id = "%s" and rating = 0.5 and verified=1) as r05;';
        query = util.format(queryproduct,productId,productId,productId,productId,productId,productId,productId,productId,productId,productId);
        result= yield databaseUtils.executeQuery(query);
       // console.log(result[0]);
       // console.log(productId);
        var rating,rated_by;
        var credit=(result[0].r5)+(result[0].r4)+(result[0].r3)+(result[0].r2)+(result[0].r1)+(result[0].r45)+(result[0].r35)+(result[0].r25)+(result[0].r15)+(result[0].r05);
        if(credit){
      // console.log("r5"+(result[0].r5)+"r4"+(result[0].r4)+"r3"+(result[0].r3)+"r2"+(result[0].r2)+"r1"+(result[0].r1));
             var avg= ((result[0].r5)*5 + (result[0].r4)*4 +(result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1+(result[0].r45)*4.5+(result[0].r35)*3.5+(result[0].r25)*2.5+(result[0].r15)*1.5+(result[0].r05)*0.5 )/(credit);
             rating=avg.toFixed(2);
             rated_by=credit;
        }
         else 
         {
           rating=0;
            rated_by=0;
         }
       // console.log(rating+"****"+rated_by);
        yield this.render('productdetail',{
        product_details : product_details,
        user_reviews : user_reviews,
        vendors_list : vendors_list,
        user_reviews_array : user_reviews_array,
        avg_rating: rating,
        rated_by:rated_by,
        r5:result[0].r5+result[0].r45,
        r4:result[0].r4+result[0].r35,
        r3:result[0].r3+result[0].r25,
        r2:result[0].r2+result[0].r15,
        r1:result[0].r1+result[0].r05,
        added:added,
        reviewid:reviewid
        
        });
        },
        addproductPage: function* (next){
            userID = this.params.slug;
            var p_id;
            yield this.render('addproduct',{
                 user: userID,
                 pid:p_id
            });
        },

        checkproduct:function* (next) {
        
            var name =this.request.body.name;
    
            var query=yield databaseUtils.executeQuery(util.format('select * from product where name="%s" and available="1"',name));
            //console.log(query[0]);
            //console.log(query);
            //console.log(query[0].p_id);
            
            if(query.length == 0){
                this.body={flag:'1',
                
                            }
            }
            else {this.body={flag:'0',
            pid:query[0].p_id
        }}
        },
            showaddproductPage: function* (next){
                var nam = this.request.body.fields.pname;
                var desc = this.request.body.fields.desc;
                var uid = this.request.body.fields.u_id;
               if(desc == "already exists")
               {
                var query='select p_id from product where name="%s";';
                query = util.format(query,nam);
                var result=yield databaseUtils.executeQuery(query);
                var id;
                    id=result[0].p_id; 
               
               }
               else{
                var uploadedFiles = this.request.body.files;
                 this.body = uploadedFiles;
                var url=this.body.img.path;
                url=url.replace("static\\static","\\static")
                var iurl= url.replace(/\\/g,'\\\\');
                var query = 'insert into product (name,description,image_url,,product_url) values ("%s","%s","%s","//");';
                            query = util.format(query,nam,desc,iurl);
                      var  result = yield databaseUtils.executeQuery(query);
                         var  purl='app/product/'+result.insertId;
                          var id= result.insertId;
                           query= 'update product set product_url = "%s" where p_id="%s";';
                           query = util.format(query,purl,result.insertId);
                          result =  yield databaseUtils.executeQuery(query);
               }                           
                //result = yield databaseUtils.executeQuery('update product set product_url='+purl);
                 query = 'insert into vendor_product (p_id,u_id,creation_timestamp) values ("%s","%s",now());';
                 query = util.format(query,id,uid);
                var result22 = yield databaseUtils.executeQuery(query);
              
                     this.redirect('/app/vendor/'+uid);
               
            },
            showAddReviewPage: function* (next){
                var productid = this.params.id;
                //console.log(productid);
                yield this.render('addreview',{
                productid: productid
                });
                },

                addReviewPage: function* (next){
                    var add= this.params.add;
                    var uid = this.currentUser.u_id;
                    var pid= this.params.id;
                    var rating = this.request.body.rating;
                    var review = this.request.body.review;
                    var rid;
                    //console.log('uid ' + uid);
                    //console.log('pid ' + pid);
                    //console.log('rating ' + rating);
                    //console.log('review '+ review);
                    //console.log("add " + add);
                    if(add == 1){
                    var queryString = 'select id from review where p_id = "%s" and u_id = "%s" and verified = "1"';
                    var queryfetchrid= util.format(queryString, pid,uid);
                    var result_rid = yield databaseUtils.executeQuery(queryfetchrid);
                    rid= result_rid[0].id;
                    console.log('rid ' + rid);
                    }
                    var queryString = 'insert into review(p_id,u_id,rating,review,creation_timestamp) values("%s","%s","%s","%s",now())';
                    var query= util.format(queryString, pid,uid,rating,review);
                    var errorMessage;
                    try{
                    var result= yield databaseUtils.executeQuery(query);
                    queryString = 'select * from review where id=%s';
                    query= util.format(queryString, result.insertId);
                    result= yield databaseUtils.executeQuery(query);
                    var insertedreview = result[0];
                    }
                    catch(e){
                    if(e.code === 'ER_DUP_ENTRY'){
                    errorMessage= 'Review already exists';
                    }else{
                    throw e;
                    }
                    }
                    if(errorMessage){
                    //console.log(errorMessage)
                    yield this.render('home',{
                    errorMessage:errorMessage
                    })
                    }else {
                    {
                    queryString = 'update review set verified= "0" where id = "%s" ';
                    var queryupdaterid= util.format(queryString, rid);
                    var results = yield databaseUtils.executeQuery(queryupdaterid);
                    //console.log(results);
                    }
                   // console.log('review added');
                    this.redirect('/app/product/'+pid);
                    }
                    }
                    ,
                    
                    showFeedbackPage: function* (next){
                    var pid= this.request.body.pid;
                    if(this.currentUser){
                    var uid = this.currentUser.u_id;
                    var id= this.request.body.id;
                    var feedback = this.request.body.feedback;
                    //console.log(uid);
                    //console.log(pid);
                   // console.log(id);
                   // console.log(feedback);
                    var queryString = 'select feedback from feedback where id= "%s" and u_id = "%s"';
                    var query= util.format(queryString,id,uid);
                    var result= yield databaseUtils.executeQuery(query);
                    var feedbackexist = result[0];
                    if(feedbackexist == undefined){
                    queryString = 'insert into feedback(id,u_id,feedback,creation_timestamp) values("%s","%s","%s",now())';
                    query= util.format(queryString, id,uid,feedback);}
                    else{
                    queryString = ' update feedback set feedback = "%s", creation_timestamp = now() where id = "%s" and u_id="%s"';
                    query= util.format(queryString, feedback,id,uid);
                    }
                    var errorMessage;
                    try{
                    result= yield databaseUtils.executeQuery(query);
                    queryString = 'select * from feedback where id=%s';
                    query= util.format(queryString, result.insertId);
                    result= yield databaseUtils.executeQuery(query);
                    }
                    catch(e){
                    if(e.code === 'ER_DUP_ENTRY'){
                    errorMessage= 'feedback already exists';
                    }else{
                    throw e;
                    }
                    }
                }else{
                    this.body='please log in';
                }
                    if(errorMessage){
                    //console.log(errorMessage)
                   this.body=errorMessage;
                    }else {
                    //console.log('feedback added');
                    this.body='feedback added';
                    
                    }
                    },

                disableProduct:function* (next){

                    var uid=this.request.body.u_id;
                    var pid=this.request.body.p_id;
                    //console.log(uid+'---------------------------------'+pid);
                 try{
                    var queryString = 'update vendor_product set available = 0 where p_id="%s" and u_id="%s";';
                    var query = util.format(queryString,pid,uid);
                    var result = yield databaseUtils.executeQuery(query);
                    //console.log(result +'    '+ query);

                    queryString='select count(*) from vendor_product  where p_id="%s" and available=1; ';
                    query = util.format(queryString,pid);
                    result = yield databaseUtils.executeQuery(query);
                   // console.log(result +'    '+ query);

                
                if(result.length==0)
                {
                    queryString='update product set available=0 where p_id="%s";';
                    query = util.format(queryString,pid);
                    result = yield databaseUtils.executeQuery(query);  
                   // console.log(result +'    '+ query);
   
                }

                
                
                }
                catch(e){

                    throw new Error(e);
                }
                this.body='product disabled';

                },
                
    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});

        this.redirect('/');
    }
}
