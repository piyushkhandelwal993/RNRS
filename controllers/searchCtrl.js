var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
     showSearchPage: function* (next) {
       var name = this.request.query.name;
       var search = this.request.query.search;
       console.log(search+"/////"+name);
       var query,result,ven=false;
       if(search =="Product")
       {
        var queryproduct = 'select product.p_id,name,image_url,count(*) as "rated_by" ,rating from product left join review on product.p_id=review.p_id where name like "%s" and product.available=1 group by p_id order by p_id desc ;'
            queryproduct = util.format(queryproduct,"%"+name+"%");
            var products_list = yield databaseUtils.executeQuery(queryproduct);
            for(var i in products_list)
            {
                
            var productId = products_list[i].p_id;
            var queryproduct =  'select (select count(*) from review where p_id = "%s" and rating = 5 and verified=1)as r5,(select count(*) from review where p_id = "%s" and rating = 4.5 and verified=1)as r45,(select count(*) from review where p_id = "%s" and rating = 4 and verified=1)as r4,(select count(*) from review where p_id = "%s" and rating = 3.5 and verified=1)as r35,(select count(*) from review where p_id = "%s" and rating = 3 and verified=1)as r3,(select count(*) from review where p_id = "%s" and rating = 2.5 and verified=1)AS r25,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 2 and verified=1)as r2,(select count(*) from review where p_id = "%s" and rating = 1.5 and verified=1)AS r15,(select count(*) from review where p_id = "%s" and rating = 1 and verified=1) as r1,(select count(*) from review where p_id = "%s" and rating = 0.5 and verified=1) as r05;';
             var query = util.format(queryproduct,productId,productId,productId,productId,productId,productId,productId,productId,productId,productId);
           
            var result= yield databaseUtils.executeQuery(query);
            var credit=(result[0].r5)+(result[0].r4)+(result[0].r3)+(result[0].r2)+(result[0].r1)+(result[0].r45)+(result[0].r35)+(result[0].r25)+(result[0].r15)+(result[0].r05);
            if(credit){
          // console.log("r5"+(result[0].r5)+"r4"+(result[0].r4)+"r3"+(result[0].r3)+"r2"+(result[0].r2)+"r1"+(result[0].r1));
                 var avg= ((result[0].r5)*5 + (result[0].r4)*4 +(result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1+(result[0].r45)*4.5+(result[0].r35)*3.5+(result[0].r25)*2.5+(result[0].r15)*1.5+(result[0].r05)*0.5 )/(credit);
                 products_list[i].rating=avg;
                 products_list[i].rated_by=credit;
            }
             else 
             {
            products_list[i].rating=0;
            products_list[i].rated_by=0;
             }
            }

            ven = false; 
            yield this.render('search',{ 
                products_list: products_list,
                isvendor: ven
        });       
       }
       else{
        var queryvendor = 'select * from vendor where firm_name like "%s"';
        var query=util.format(queryvendor,"%"+name+"%");
    ven=true;
    var result= yield databaseUtils.executeQuery(query);
    
    yield this.render('search',{ 
        products_list: result,
        isvendor: ven
});
    }
},
}


