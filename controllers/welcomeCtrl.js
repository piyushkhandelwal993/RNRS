var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
    showAboutusPage: function* (next) {
        yield this.render('aboutus',{
        });
        
        },
        
        showOurteamPage: function* (next) {
        yield this.render('ourteam',{
        });
        
        },
        
        showAcknowledgementPage: function* (next) {
        yield this.render('acknowledgement',{
        });
        
        },
        
   showHomePage: function* (next) {

    // top 3 most reviewed / rated products
        var queryString_most_review ='SELECT p.p_id,p.name,p.description, p.image_url, COUNT(*) as "rated_by", AVG(r.rating) as "rating" FROM product AS p LEFT JOIN review AS r ON p.p_id = r.p_id where r.review is not null and p.available=1 GROUP BY p.p_id ORDER BY count(*) DESC limit 3';
        var products_list_most_review =yield databaseUtils.executeQuery(queryString_most_review);
        for(var i in products_list_most_review){
            //console.log("*-----------"+i+"-------------"+products_list_most_review[i].p_id+"----------"+products_list_most_review[i].rated_by+"-----------"+products_list_most_review[i].rating);
        
        var productId = products_list_most_review[i].p_id;
        var queryproduct =  'select (select count(*) from review where p_id = "%s" and rating = 5 and verified=1)as r5,(select count(*) from review where p_id = "%s" and rating = 4.5 and verified=1)as r45,(select count(*) from review where p_id = "%s" and rating = 4 and verified=1)as r4,(select count(*) from review where p_id = "%s" and rating = 3.5 and verified=1)as r35,(select count(*) from review where p_id = "%s" and rating = 3 and verified=1)as r3,(select count(*) from review where p_id = "%s" and rating = 2.5 and verified=1)AS r25,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 2 and verified=1)as r2,(select count(*) from review where p_id = "%s" and rating = 1.5 and verified=1)AS r15,(select count(*) from review where p_id = "%s" and rating = 1 and verified=1) as r1,(select count(*) from review where p_id = "%s" and rating = 0.5 and verified=1) as r05;';
        query = util.format(queryproduct,productId,productId,productId,productId,productId,productId,productId,productId,productId,productId);
        result= yield databaseUtils.executeQuery(query);
      //  console.log(result[0]);
      //  console.log(productId);
        
        var credit=(result[0].r5)+(result[0].r4)+(result[0].r3)+(result[0].r2)+(result[0].r1)+(result[0].r45)+(result[0].r35)+(result[0].r25)+(result[0].r15)+(result[0].r05);
        if(credit){
      // console.log("r5"+(result[0].r5)+"r4"+(result[0].r4)+"r3"+(result[0].r3)+"r2"+(result[0].r2)+"r1"+(result[0].r1));
             var avg= ((result[0].r5)*5 + (result[0].r4)*4 +(result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1+(result[0].r45)*4.5+(result[0].r35)*3.5+(result[0].r25)*2.5+(result[0].r15)*1.5+(result[0].r05)*0.5 )/(credit);
             products_list_most_review[i].rating=avg.toFixed(2);
             products_list_most_review[i].rated_by=credit;
        }
         else 
         {
           rating=0;
            rated_by=0;
         }

       // top 3 distributers

        var queryString_top_distributers='SELECT v.u_id,v.firm_name,v.logo, v.location, COUNT(*) as "added_product" FROM vendor AS v LEFT JOIN vendor_product AS vp ON v.u_id = vp.u_id GROUP BY v.u_id ORDER BY count(*) DESC LIMIT 3';
        var result3=yield databaseUtils.executeQuery(queryString_top_distributers);
 
// all products - recently added
var queryproduct = 'select product.p_id,product.description,name,image_url,count(*) as "rated_by" ,rating from product left join review on product.p_id=review.p_id and product.available=1 group by p_id order by p_id desc ;'
var products_list = yield databaseUtils.executeQuery(queryproduct);
for(var i in products_list)
{
    //console.log("-----------"+i+"-------------"+products_list[i].p_id+"----------"+products_list[i].rated_by+"-----------"+products_list[i].rating);
    
    var productId = products_list[i].p_id;
    var queryproduct =  'select (select count(*) from review where p_id = "%s" and rating = 5 and verified=1)as r5,(select count(*) from review where p_id = "%s" and rating = 4.5 and verified=1)as r45,(select count(*) from review where p_id = "%s" and rating = 4 and verified=1)as r4,(select count(*) from review where p_id = "%s" and rating = 3.5 and verified=1)as r35,(select count(*) from review where p_id = "%s" and rating = 3 and verified=1)as r3,(select count(*) from review where p_id = "%s" and rating = 2.5 and verified=1)AS r25,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 2 and verified=1)as r2,(select count(*) from review where p_id = "%s" and rating = 1.5 and verified=1)AS r15,(select count(*) from review where p_id = "%s" and rating = 1 and verified=1) as r1,(select count(*) from review where p_id = "%s" and rating = 0.5 and verified=1) as r05;';
     var query = util.format(queryproduct,productId,productId,productId,productId,productId,productId,productId,productId,productId,productId);
   
    var result= yield databaseUtils.executeQuery(query);
    var credit=(result[0].r5)+(result[0].r4)+(result[0].r3)+(result[0].r2)+(result[0].r1)+(result[0].r45)+(result[0].r35)+(result[0].r25)+(result[0].r15)+(result[0].r05);
    if(credit){
  // console.log("r5"+(result[0].r5)+"r4"+(result[0].r4)+"r3"+(result[0].r3)+"r2"+(result[0].r2)+"r1"+(result[0].r1));
         var avg= ((result[0].r5)*5 + (result[0].r4)*4 +(result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1+(result[0].r45)*4.5+(result[0].r35)*3.5+(result[0].r25)*2.5+(result[0].r15)*1.5+(result[0].r05)*0.5 )/(credit);
         products_list[i].rating=avg.toFixed(2);
         products_list[i].rated_by=credit;
    }
     else 
     {
    products_list[i].rating=0;
    products_list[i].rated_by=0;
     }
      //   console.log("****"+products_list[i].rating);
}

    var sortedArray = [];
    for(var i in products_list)
    {
        // Push each JSON Object entry in array by [value, key]
        sortedArray.push([products_list[i].rating, products_list[i].p_id]);
    }
    var products_list_sorted=sortedArray.sort().reverse();

    var id=[products_list_sorted[0][1],products_list_sorted[1][1],products_list_sorted[2][1]];
   // console.log(id);
   // console.log(products_list_most_review);
    var products_list_top_rated=[];
    for(var i in products_list)
    {
        //console.log(products_list[i].p_id+"+++"+id[0]+"+++"+id[1]+"+++"+id[2]);
        if(products_list[i].p_id==id[0]||products_list[i].p_id==id[1]||products_list[i].p_id==id[2])
        {
            products_list_top_rated.push({ p_id: products_list[i].p_id,
                name: products_list[i].name,
                image_url: products_list[i].image_url,
                rated_by: products_list[i].rated_by,
                rating: products_list[i].rating});
        }
    }
    //console.log(products_list_top_rated);
        yield this.render('home',{
            showDetails:false,
            userDetails1:products_list_most_review,
            userDetails2:products_list_top_rated,
            userDetails3:result3,
            products_list:products_list
        });
    }
},

forgotpassword: function* (next) {
    var errorMessage;
    yield this.render('forgotPassword',{
        errorMessage:errorMessage

    });
        

},

pass: function* (next) {
    var pws = this.request.body.psd;
    var emai = this.request.body.email;
      console.log(pws);

      console.log("------"+emai);
 var queryString = 'update user set password="%s" where user_email="%s"';
    var query = util.format(queryString,pws,emai);
    var results = yield databaseUtils.executeQuery(query);
    this.redirect("/app/home");
},
checkotp: function* (next) {
var email=this.request.body.email;
var otp = this.request.body.otp;
    console.log(otp);
    console.log("+++++++++++++++++++++"+email);
    var queryString2= 'DELETE FROM otp WHERE time < (CURTIME() - INTERVAL 2 MINUTE)';
    var query1 = util.format(queryString2);
    var result2 = yield databaseUtils.executeQuery(query1);



var queryString = 'select * from otp where otp = "%s"';
var query = util.format(queryString,otp);
var result = yield databaseUtils.executeQuery(query);
var userDetails=result[0];
var errorMessage;
if(result.length == 0) {
queryString = 'select * from user where user_email= "%s"';
query = util.format(queryString,email);
result = yield databaseUtils.executeQuery(query);
var user=result[0];
  error = "wrong otp";
  //console.log(user);
  yield this.render('otp',{
  userDetail:user,
  errorMessage:error
   });
       }else {
yield this.render('newpass',{
userDetails:userDetails
});

}
 }
}
