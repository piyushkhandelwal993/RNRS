var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
     showCustomerDetailPage: function* (next) {
        var userId = this.params.id;
        var queryString ='select * from user where u_id ="%s"';
        var query=util.format(queryString,userId);
        var result=yield databaseUtils.executeQuery(query);
        var user_details = result[0];

        var queryString_last_ratedreviewed ='SELECT p.p_id,p.name, p.image_url, COUNT(*) as "rated_by", AVG(r.rating) as "rating" FROM product AS p LEFT JOIN review AS r ON p.p_id = r.p_id WHERE r.p_id IN (SELECT r.p_id FROM review GROUP BY r.u_id HAVING r.u_id = "%s" ORDER BY creation_timestamp) and available=1 GROUP BY p.p_id ORDER BY count(*)';
        queryString_last_ratedreviewed=util.format(queryString_last_ratedreviewed,userId);
        var products_list_last_ratedreviewed =yield databaseUtils.executeQuery(queryString_last_ratedreviewed);
        for(var i in products_list_last_ratedreviewed){

            var productId = products_list_last_ratedreviewed[i].p_id;
            var queryproduct =  'select (select count(*) from review where p_id = "%s" and rating = 5 and verified=1)as r5,(select count(*) from review where p_id = "%s" and rating = 4.5 and verified=1)as r45,(select count(*) from review where p_id = "%s" and rating = 4 and verified=1)as r4,(select count(*) from review where p_id = "%s" and rating = 3.5 and verified=1)as r35,(select count(*) from review where p_id = "%s" and rating = 3 and verified=1)as r3,(select count(*) from review where p_id = "%s" and rating = 2.5 and verified=1)AS r25,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 2 and verified=1)as r2,(select count(*) from review where p_id = "%s" and rating = 1.5 and verified=1)AS r15,(select count(*) from review where p_id = "%s" and rating = 1 and verified=1) as r1,(select count(*) from review where p_id = "%s" and rating = 0.5 and verified=1) as r05;';
             var query = util.format(queryproduct,productId,productId,productId,productId,productId,productId,productId,productId,productId,productId);

            var result= yield databaseUtils.executeQuery(query);
            var credit=(result[0].r5)+(result[0].r4)+(result[0].r3)+(result[0].r2)+(result[0].r1)+(result[0].r45)+(result[0].r35)+(result[0].r25)+(result[0].r15)+(result[0].r05);
            if(credit){
          // console.log("r5"+(result[0].r5)+"r4"+(result[0].r4)+"r3"+(result[0].r3)+"r2"+(result[0].r2)+"r1"+(result[0].r1));
                 var avg= ((result[0].r5)*5 + (result[0].r4)*4 +(result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1+(result[0].r45)*4.5+(result[0].r35)*3.5+(result[0].r25)*2.5+(result[0].r15)*1.5+(result[0].r05)*0.5 )/(credit);
                 products_list_last_ratedreviewed[i].rating=avg;
                 products_list_last_ratedreviewed[i].rated_by=credit;
            }
             else
             {
            products_list_last_ratedreviewed[i].rating=0;
            products_list_last_ratedreviewed[i].rated_by=0;
             }
        }
        yield this.render('customerdetailpage',{
            reviewed_products_list:products_list_last_ratedreviewed,
            user_details:user_details,

        });
    },
    showAllCustomers: function* (next){
                var query = 'select * from user join user_role on user.u_id=user_role.u_id where user_role.r_id=1;';
                var result = yield databaseUtils.executeQuery(query);
                yield this.render('customers',{
                    customers_list: result
                })
    },
    showVendorsPage: function* (next){
        var query = 'select * from vendor;';
        var vendors_list = yield databaseUtils.executeQuery(query);
        yield this.render('vendors',{
            vendors_list: vendors_list
        });
    },
    changepassword: function* (next){

        var oldpassword= this.request.body.password;
        var mob=this.request.body.mob;
        var password = this.request.body.psw;

        var queryString = 'select * from user where mob="%s"';
        var query= util.format(queryString, mob);
        var result = yield databaseUtils.executeQuery(query);

        var errorMessage;
        if(result.length == 0)
        {
        errorMessage = "ERROR";
        throw new Error("Mobile number not found");
        }
        else{
        var user=result[0];
        if(user.password ===oldpassword)
        {
        //sessionUtils.saveUserInSession(user,this.cookies);
        queryString='update user set password ="%s" where mob="%s" ;';
        query= util.format(queryString, password,mob);
        console.log(query);
        result = yield databaseUtils.executeQuery(query);
errorMessage='password reset';
        }
        }
    this.body=errorMessage;
    this.redirect('/app/profilepage/'+user.u_id);
        },



        editprofile:function* (next){
           var uploadedFiles = this.request.body.files;
            var mob=this.request.body.fields.mob;
            var name=this.request.body.fields.name;
            var gender=this.request.body.fields.gender;
            var user_email=this.request.body.fields.user_email;
            var dob =this.request.body.fields.dob;
            this.body = uploadedFiles;
            var url=this.body.img.path;
            var size=this.body.img.size;
            url=url.replace("static\\static","\\static")
            var furl= url.replace(/\\/g,'\\\\');

            var queryString = 'select * from user where mob="%s"';
            var query= util.format(queryString, mob);
            var result = yield databaseUtils.executeQuery(query);
            var u_id=result[0].u_id;
            console.log(query);
            var errorMessage;
            if(result.length == 0 ){
            errorMessage = "ERROR";

            }
            else{

                if(name){

                    queryString='update user set name ="%s" where mob="%s" ;';
                    query= util.format(queryString,name,mob);
                    result = yield databaseUtils.executeQuery(query);

                }
                if(user_email)
                {
                    console.log("-------------------------------email-------------------------");
                    queryString='update user set user_email ="%s" where mob="%s" ;';
                    query= util.format(queryString,user_email,mob);
                    result = yield databaseUtils.executeQuery(query);
                }
                if(dob)
                {
                    console.log("-------------------------------dob--------------------------");
                    queryString='update user set dob ="%s" where mob="%s" ;';
                    query= util.format(queryString,dob,mob);
                    result = yield databaseUtils.executeQuery(query);
                }
                if(gender)
                {
                    console.log("-------------------------------gender--------------------------");
                    queryString='update user set gender ="%s" where mob="%s" ;';
                    query= util.format(queryString,gender,mob);
                    result = yield databaseUtils.executeQuery(query);
                }
                if(furl && size != 0)
                {
                    console.log("-------------------------------img--------------------------");
                    queryString='update user set img ="%s" where mob="%s" ;';
                    query= util.format(queryString,furl,mob);
                    result = yield databaseUtils.executeQuery(query);

                }

var crt_user='select * from user join user_role on user.u_id=user_role.u_id where mob="%s"';
var c_query=util.format(crt_user,mob);

var rst=yield databaseUtils.executeQuery(c_query);
sessionUtils.updateUserInSession(rst[0],this.cookies);


this.redirect('/app/profile/'+u_id); }
            },
            editvendorprofile:function* (next){
                var uploadedFiles = this.request.body.files;
                 var uid =this.request.body.fields.uid;
                 var firm=this.request.body.fields.firmname;
                 var location=this.request.body.fields.location;
                 var panno=this.request.body.fields.panno;
                 var gstno =this.request.body.fields.gstno;
                this.body=uploadedFiles;
                 var logourl=this.body.firmlogo.path;
                 var logosize=this.body.firmlogo.size;
                 logourl=logourl.replace("static\\static","\\static")
                 logourl= logourl.replace(/\\/g,'\\\\');

                 var gsturl = this.body.gstcerti.path;
                 var gstsize=this.body.gstcerti.size;
                 gsturl=gsturl.replace("static\\static","\\static")
                 gsturl= gsturl.replace(/\\/g,'\\\\');

                 var queryString;
                 var query;
                 var result;
                     if(firm){

                         queryString='update vendor set firm_name ="%s" where u_id="%s" ;';
                         query= util.format(queryString,firm,uid);
                         result = yield databaseUtils.executeQuery(query);

                     }
                     if(location)
                     {

                         queryString='update vendor set location ="%s" where u_id="%s" ;';
                         query= util.format(queryString,location,uid);
                         result = yield databaseUtils.executeQuery(query);
                     }
                     if(panno)
                     {
                         queryString='update vendor set pan_no ="%s" where u_id="%s" ;';
                         query= util.format(queryString,panno,uid);
                         result = yield databaseUtils.executeQuery(query);
                     }
                     if(gstno)
                     {

                         queryString='update vendor set gst_no ="%s" where u_id="%s" ;';
                         query= util.format(queryString,gstno,uid);
                         result = yield databaseUtils.executeQuery(query);
                     }
                     if(logourl&& logosize != 0)
                     {

                         queryString='update vendor set logo ="%s" where u_id="%s" ;';
                         query= util.format(queryString,logourl,uid);
                         result = yield databaseUtils.executeQuery(query);

                     }
                     if(gsturl && gstsize != 0)
                     {

                         queryString='update vendor set gst_certi ="%s" where u_id="%s" ;';
                         query= util.format(queryString,gsturl,uid);
                         result = yield databaseUtils.executeQuery(query);

                     }
                     this.redirect('/app/profile/'+uid);

    /* var crt_user='select * from user join user_role on user.u_id=user_role.u_id where mob="%s"';
     var c_query=util.format(crt_user,mob);

     var rst=yield databaseUtils.executeQuery(c_query);
     console.log(rst);
     sessionUtils.updateUserInSession(rst[0],this.cookies);*/



                 },
        login: function* (next){

            var username= this.request.body.mob;
            console.log(username);
            var password = this.request.body.psw;
            var queryString = 'select * from user join user_role on user.u_id= user_role.u_id where mob="%s"';
            var query= util.format(queryString, username);
            var result = yield databaseUtils.executeQuery(query);

            var errorMessage = 'loged in';
            console.log(result[0]);
            console.log(errorMessage+result.length);
            if(result.length == 0)
            {
                errorMessage = "ERROR";
                console.log(errorMessage);
                    this.body=errorMessage;
            } else{
                var user=result[0];
                if(user.password === password)
                {
                    sessionUtils.saveUserInSession(user,this.cookies);

                }else{
                errorMessage = "WRONG PASSWORD";


                    }
            }
            this.body=errorMessage;
            },

        logout: function* (next) {
            var sessionId = this.cookies.get("SESSION_ID");
            if(sessionId) {
                sessionUtils.deleteSession(sessionId);
            }
            this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});

            this.redirect('/app/home');
        },


        signup:  function* (next) {
            console.log('inside sign up');
            var uploadedFile = this.request.body.files;
            console.log(uploadedFile);
           var logo_url=uploadedFile.logo.path;
           var gst_url=uploadedFile.gst_certi.path;
            var errorMessage="" ;
            var result;
          try{
              console.log(logo_url);
                logo_url=logo_url.replace("static\\static","\\static")
                logo_url = logo_url.replace(/\\/g,'\\\\');
                var firmname=this.request.body.fields.fname;
                var location=this.request.body.fields.location;
                var gst_no=this.request.body.fields.gst_no;
                var pan=this.request.body.fields.pan;

                gst_url=gst_url.replace("static\\static","\\static")
                    gst_url = gst_url.replace(/\\/g,'\\\\');




                var user_details = 'update user_role set r_id=2 where u_id="%s";';
                 var   query = util.format(user_details,this.currentUser.u_id);
                yield databaseUtils.executeQuery(query);




                 var vendor_details = 'insert into vendor(firm_name,location,logo,gst_no,pan_no,gst_certi,u_id) values("%s","%s","%s","%s","%s","%s","%s");';
                 query = util.format(vendor_details,firmname,location,logo_url,gst_no,pan,gst_url,this.currentUser.u_id);
             result = yield databaseUtils.executeQuery(query);
             this.redirect('/app/home');

        }
        catch(e){
            if(e.code === 'ER_DUP_ENTRY'){
            errorMessage= 'User already exists';
            }
            else
            {
            throw e;
            }
            }
            if(errorMessage){
            console.log(errorMessage);
            yield this.render('signupPage',{
            errorMessage:errorMessage
            });
        }


        var crt_user='select * from user join user_role on user.u_id=user_role.u_id where user.u_id="%s"';
var c_query=util.format(crt_user,this.currentUser.u_id);

var rst=yield databaseUtils.executeQuery(c_query);
console.log(rst);
sessionUtils.updateUserInSession(rst[0],this.cookies);
   },




   signupascustomer:  function* (next) {

            console.log('sign up as customer');
            var name=this.request.body.name;
            var mob_no=this.request.body.mob;
            var password=this.request.body.psw;
            var email=this.request.body.email;
           var user_details = 'insert into user(name,mob,password,user_email,creation_timestamp) values("%s","%s","%s","%s",now());';
           var errorMessage = '';
          var query = util.format(user_details,name,mob_no,password,email);
          try{
          var result = yield databaseUtils.executeQuery(query);
            var user_details = 'insert into user_role(u_id,r_id) values("%s",1);';
            var quer = util.format(user_details,result.insertId);
             resul = yield databaseUtils.executeQuery(quer);
              errorMessage="signed up sucessfully" ;
             this.body = errorMessage;
            }
        catch(e){
            if(e.code === 'ER_DUP_ENTRY'){
            errorMessage= 'User already exists';
            throw new Error("User information already exit");
            }
            else
            {
            throw e;
            }
            }
            if(errorMessage){
            this.body=errorMessage;

        }
   },


         showsignupasVendorPage: function* (next) {
            yield this.render('signupasVendorPage',{
            });
            },

            showAdminPage: function* (next) {
                yield this.render('adminPage',{
                });
                },



                showVendorDetailPage: function* (next){
                    userId= this.params.u_id;
                    var queryString = 'select * from vendor join user on vendor.u_id=user.u_id where vendor.u_id="%s";';
                    var query= util.format(queryString, userId);
                    var result = yield databaseUtils.executeQuery(query);
                    var vendor_details=result[0];

                    queryString = 'select product.p_id,name,image_url,vendor_product.p_id,avg(rating) as rating, count(review.id) as rated_by from ((product join vendor_product on vendor_product.p_id=product.p_id) left join review on review.p_id=vendor_product.p_id) where vendor_product.u_id="%s" and vendor_product.available=1 group by product.p_id;'
                    query = util.format(queryString, userId);
                    result = yield databaseUtils.executeQuery(query);
                    var vendor_products_list = result;


                    for(var i in vendor_products_list)
                    {
                        var productId = vendor_products_list[i].p_id;
                        var queryproduct =  'select (select count(*) from review where p_id = "%s" and rating = 5 and verified=1)as r5,(select count(*) from review where p_id = "%s" and rating = 4.5 and verified=1)as r45,(select count(*) from review where p_id = "%s" and rating = 4 and verified=1)as r4,(select count(*) from review where p_id = "%s" and rating = 3.5 and verified=1)as r35,(select count(*) from review where p_id = "%s" and rating = 3 and verified=1)as r3,(select count(*) from review where p_id = "%s" and rating = 2.5 and verified=1)AS r25,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 2 and verified=1)as r2,(select count(*) from review where p_id = "%s" and rating = 1.5 and verified=1)AS r15,(select count(*) from review where p_id = "%s" and rating = 1 and verified=1) as r1,(select count(*) from review where p_id = "%s" and rating = 0.5 and verified=1) as r05;';
                         var query = util.format(queryproduct,productId,productId,productId,productId,productId,productId,productId,productId,productId,productId);

                        var result= yield databaseUtils.executeQuery(query);
                        var credit=(result[0].r5)+(result[0].r4)+(result[0].r3)+(result[0].r2)+(result[0].r1)+(result[0].r45)+(result[0].r35)+(result[0].r25)+(result[0].r15)+(result[0].r05);
                        if(credit)
                        {

                             var avg= ((result[0].r5)*5 + (result[0].r4)*4 +(result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1+(result[0].r45)*4.5+(result[0].r35)*3.5+(result[0].r25)*2.5+(result[0].r15)*1.5+(result[0].r05)*0.5 )/(credit);
                             vendor_products_list[i].rating=avg.toFixed(2);
                             vendor_products_list[i].rated_by=credit;
                        }
                         else
                         {
                            vendor_products_list[i].rating=0;
                            vendor_products_list[i].rated_by=0;
                         }


                    }


                    queryString = ' select count(*) as no from vendor_product where u_id="%s" and available=1; ';
                    query = util.format(queryString, userId);
                    result = yield databaseUtils.executeQuery(query);
                    var produuct_count = result[0];


                    yield this.render('vendor_detailpage',{
                      vendor_details : vendor_details,
                      vendor_products_list : vendor_products_list,
                      produuct_count:produuct_count
                    });
                },
    checkmobs:function* (next) {
        var mob =this.request.body.mob;

        var query=yield databaseUtils.executeQuery(util.format('select * from user where mob="%s"',mob));
        if(query.length == 0){
            this.body={flag:'1'}
        }
        else this.body={flag:'0'}
    },
    checkproducts:function* (next) {
        var name =this.request.body.name;

        var query=yield databaseUtils.executeQuery(util.format('select * from product where name="%s"',name));
        if(query.length == 0){
            this.body={flag:'1'}
        }
        else this.body={flag:'0'}
    },
    checkfirmname:function* (next) {
        var fname =this.request.body.fname;

        var query=yield databaseUtils.executeQuery(util.format('select * from vendor where firm_name="%s"',fname));
        if(query.length == 0){
            this.body={flag:'1'}
        }
        else this.body={flag:'0'}
    },
    showsignupasVendorPage: function* (next){
        yield this.render('signupasVendorPage',{


        });
    },
    signupasVendor:  function* (next) {
        console.log('inside sign up');
        var uploadedFile = this.request.body.files;
        console.log(uploadedFile);
       var logo_url=uploadedFile.logo.path;
       var gst_url=uploadedFile.gst_certi.path;
        var errorMessage="" ;
        var result;
      try{
          console.log(logo_url);
            logo_url=logo_url.replace("static\\static","\\static")
            logo_url = logo_url.replace(/\\/g,'\\\\');
            var firmname=this.request.body.fields.fname;
            var location=this.request.body.fields.location;
            var gst_no=this.request.body.fields.gst_no;
            var pan=this.request.body.fields.pan;

            gst_url=gst_url.replace("static\\static","\\static")
                gst_url = gst_url.replace(/\\/g,'\\\\');




            var user_details = 'update user_role set r_id=2 where u_id="%s";';
             var   query = util.format(user_details,this.currentUser.u_id);
            yield databaseUtils.executeQuery(query);




             var vendor_details = 'insert into vendor(firm_name,location,logo,gst_no,pan_no,gst_certi,u_id) values("%s","%s","%s","%s","%s","%s","%s");';
             query = util.format(vendor_details,firmname,location,logo_url,gst_no,pan,gst_url,this.currentUser.u_id);
         result = yield databaseUtils.executeQuery(query);
         this.redirect('/app/home');

    }
    catch(e){
        if(e.code === 'ER_DUP_ENTRY'){
        errorMessage= 'User already exists';
        }
        else
        {
        throw e;
        }
        }
        if(errorMessage){
        console.log(errorMessage);
        yield this.render('signupPage',{
        errorMessage:errorMessage
        });
    }


    var crt_user='select * from user join user_role on user.u_id=user_role.u_id where user.u_id="%s"';
var c_query=util.format(crt_user,this.currentUser.u_id);

var rst=yield databaseUtils.executeQuery(c_query);
console.log(rst);
sessionUtils.updateUserInSession(rst[0],this.cookies);
},
    showprofilepage: function* (next){
        var userId = this.params.id;
        var queryString ='select * from user where u_id ="%s"';
        var query=util.format(queryString,userId);
        var result=yield databaseUtils.executeQuery(query);
        var user_details = result[0];

         queryString ='select * from vendor where u_id ="%s"';
         query=util.format(queryString,userId);
         result=yield databaseUtils.executeQuery(query);
         var vendors_details = result[0];


        var queryproduct = 'select product.p_id,name,image_url,count(*) as "rated_by" ,rating,review,creation_timestamp from product left join review on product.p_id=review.p_id where product.available=1 and product.p_id in (SELECT product.p_id FROM product JOIN vendor_product ON product.p_id=vendor_product.p_id where u_id="%s" and vendor_product.available=1 order by creation_timestamp desc) group by p_id order by p_id desc ;'
        queryproduct=util.format(queryproduct,userId);
        var products_list = yield databaseUtils.executeQuery(queryproduct);
        for(var i in products_list)
        {
            //console.log("-----------"+i+"-------------"+products_list[i].p_id+"----------"+products_list[i].rated_by+"-----------"+products_list[i].rating);


            var productId = products_list[i].p_id;
            var queryproduct =  'select (select count(*) from review where p_id = "%s" and rating = 5 and verified=1)as r5,(select count(*) from review where p_id = "%s" and rating = 4.5 and verified=1)as r45,(select count(*) from review where p_id = "%s" and rating = 4 and verified=1)as r4,(select count(*) from review where p_id = "%s" and rating = 3.5 and verified=1)as r35,(select count(*) from review where p_id = "%s" and rating = 3 and verified=1)as r3,(select count(*) from review where p_id = "%s" and rating = 2.5 and verified=1)AS r25,(SELECT COUNT(*) FROM review where p_id = "%s" and rating = 2 and verified=1)as r2,(select count(*) from review where p_id = "%s" and rating = 1.5 and verified=1)AS r15,(select count(*) from review where p_id = "%s" and rating = 1 and verified=1) as r1,(select count(*) from review where p_id = "%s" and rating = 0.5 and verified=1) as r05;';
             var query = util.format(queryproduct,productId,productId,productId,productId,productId,productId,productId,productId,productId,productId);

            var result= yield databaseUtils.executeQuery(query);
            var credit=(result[0].r5)+(result[0].r4)+(result[0].r3)+(result[0].r2)+(result[0].r1)+(result[0].r45)+(result[0].r35)+(result[0].r25)+(result[0].r15)+(result[0].r05);
            if(credit){
          // console.log("r5"+(result[0].r5)+"r4"+(result[0].r4)+"r3"+(result[0].r3)+"r2"+(result[0].r2)+"r1"+(result[0].r1));
                 var avg= ((result[0].r5)*5 + (result[0].r4)*4 +(result[0].r3)*3    +   (result[0].r2)*2    +   (result[0].r1)*1+(result[0].r45)*4.5+(result[0].r35)*3.5+(result[0].r25)*2.5+(result[0].r15)*1.5+(result[0].r05)*0.5 )/(credit);
                 products_list[i].rating=avg.toFixed(2);
                 products_list[i].rated_by=credit;
            }
             else
             {
            products_list[i].rating=0;
            products_list[i].rated_by=0;
             }
        }


        queryString='SELECT p.name, p.image_url, r.rating,r.review ,r.creation_timestamp FROM product AS p JOIN review AS r ON p.p_id = r.p_id where r.u_id="%s" order by r.creation_timestamp desc';
        query=util.format(queryString,userId);
        result=yield databaseUtils.executeQuery(query);
        var reviewed_products_list = result;

        yield this.render('userprofile',{
            reviewed_products_list:reviewed_products_list,
            user_details:user_details,
            vendor_details: vendors_details,
            recent_products:products_list.reverse()

        });
    }

}
